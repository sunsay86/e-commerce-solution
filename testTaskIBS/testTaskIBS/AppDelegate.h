//
//  AppDelegate.h
//  testTaskIBS
//
//  Created by Александр Волков on 19.04.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


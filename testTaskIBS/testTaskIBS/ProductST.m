//
//  ProductST.m
//  testTaskIBS
//
//  Created by Александр Волков on 23.04.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductST.h"

@implementation ProductST


@synthesize list;
- (NSMutableArray *) list
{
    if (!list) list = [[NSMutableArray alloc] init];
    return list;
    
}


static ProductST *sharedProductST = NULL;
+(ProductST *)sharedProductST {
    if (!sharedProductST || sharedProductST == NULL) {
        sharedProductST = [ProductST new];
        
    }
    return sharedProductST;
    
}
-(void)addProduct:(Product *)product
{
    [self.list addObject: product];
}


-(void)removeProductAtIndex:(NSInteger *)index
{
    
    if ([self.list count]) {
        [self.list removeObjectAtIndex: index];
        
    }
    
}

-(void)replaceProduct:(Product *)product atIndex:(NSNumber *)index
{
    if ([self.list count]) {
        [self.list replaceObjectAtIndex:[index integerValue] withObject:product];
    }
    
}

-(void)buyProduct:(Product *)product atIndex:(NSNumber *)index
{
    
    if ([self.list count]) {
        int count = [product.count intValue];
        if ( count > 1) {
            product.count = @(count - 1).stringValue;
            
        } else {
            
            [self.list removeObjectAtIndex: [index integerValue]];
        }
        
    }
     
}

- (void)dealloc {
    self.list = nil;
    //[super dealloc];
}

@end

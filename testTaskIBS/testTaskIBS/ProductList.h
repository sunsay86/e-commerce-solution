//
//  ProductList.h
//  testTaskIBS
//
//  Created by Александр Волков on 19.04.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

#ifndef ProductList_h
#define ProductList_h


#endif /* ProductList_h */


#import <Foundation/Foundation.h>
#import "Product.h"


@interface ProductList : NSObject
@property (strong, nonatomic) NSMutableArray * list;

-(void)addProduct:(Product *)product;
-(void)removeProduct:(Product *)product;



@end

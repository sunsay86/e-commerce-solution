//
//  Product_h
//  testTaskIBS
//
//  Created by Александр Волков on 19.04.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

#ifndef Product_h
#define Product_m


#endif /* Product_m */


#import <Foundation/Foundation.h>

@interface Product : NSObject

@property (nonatomic, strong, readwrite) NSString* name;
@property (nonatomic, strong, readwrite) NSString* category;
@property (nonatomic, strong, readwrite) NSString* price;
@property (nonatomic, strong, readwrite) NSString* count;







@end

//
//  ProductList.m
//  testTaskIBS
//
//  Created by Александр Волков on 19.04.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductList.h"

@interface ProductList()





@end

@implementation ProductList

- (NSMutableArray *) list
{
    if (!_list) _list = [[NSMutableArray alloc] init];
    return _list;
    
}


-(void)addProduct:(Product *)product
{
    [self.list addObject: product];
}


-(void)removeProduct:(Product *)product
{
    
    if ([self.list count]) {
        [self.list removeObject: product];
        
    }
    
}


@end

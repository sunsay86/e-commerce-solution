//
//  SellTableViewController.h
//  testTaskIBS
//
//  Created by Александр Волков on 20.04.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"
#import "ProductST.h"

@interface SellTableViewController : UITableViewController
@property (strong,nonatomic,readwrite) Product *product;


@end

//
//  ProductST.h
//  testTaskIBS
//
//  Created by Александр Волков on 23.04.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

#ifndef ProductST_h
#define ProductST_h


#endif /* ProductST_h */
#import <Foundation/Foundation.h>
#import "Product.h"
@interface ProductST: NSObject {
    NSMutableArray *list;
    
    
}
@property (nonatomic, retain) NSMutableArray *list;

-(void)addProduct:(Product *)product;
-(void)removeProductAtIndex:(NSInteger *)index;
-(void)replaceProduct:(Product *)product atIndex:(NSNumber *)index;
-(void)buyProduct:(Product *)product atIndex:(NSNumber *)index;
+(ProductST *)sharedProductST;


@end

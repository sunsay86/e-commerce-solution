//
//  DetailSellTableViewController.m
//  testTaskIBS
//
//  Created by Александр Волков on 22.04.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

#import "DetailSellTableViewController.h"

#import "SellTableViewController.h"

@interface DetailSellTableViewController ()
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *categoryTextField;
@property (weak, nonatomic) IBOutlet UITextField *priceTextField;
@property (weak, nonatomic) IBOutlet UITextField *countTextField;
@end

@implementation DetailSellTableViewController


- (Product*)saveNewProduct {
    _product.name = _nameTextField.text;
    _product.category = _categoryTextField.text;
    _product.price = _priceTextField.text;
    _product.count = _countTextField.text;
    
    return _product;
}

-(void)updateUI {
    _nameTextField.text = _product.name;
    _categoryTextField.text = _product.category ;
    _priceTextField.text = _product.price;
    _countTextField.text = _product.count ;
    
}

-(BOOL)isValid {
    if (_nameTextField.hasText &&
        _categoryTextField.hasText &&
        _priceTextField.hasText &&
        _countTextField.hasText){
        return true;
    }
    return false;
}

- (IBAction)save:(UIBarButtonItem *)sender {
    if ([self isValid]) {
        [self saveNewProduct];
        if (self.tag == nil) {
            
            [[ProductST sharedProductST] addProduct:_product];

        } else {
            
            [[ProductST sharedProductST] replaceProduct:_product atIndex:_tag];
        
        }
        
        [self performSegueWithIdentifier:@"unwind to sell" sender:sender];
        
       
        
    } else {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Ошибка!" message: @"Все поля должны быть заполнены!" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Отмена" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:true completion:nil];
        
    }
    
}
- (IBAction)cancel:(UIBarButtonItem *)sender {
    [self performSegueWithIdentifier:@"unwind to sell" sender:sender];
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
           [self updateUI];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return 0;
}
*/
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

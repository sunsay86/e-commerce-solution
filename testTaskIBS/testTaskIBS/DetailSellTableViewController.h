//
//  DetailSellTableViewController.h
//  testTaskIBS
//
//  Created by Александр Волков on 22.04.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"

#import "ProductST.h"
@interface DetailSellTableViewController : UITableViewController
@property (strong,nonatomic,readwrite) Product *product;
@property (strong,nonatomic,readwrite) NSNumber *tag;

@end

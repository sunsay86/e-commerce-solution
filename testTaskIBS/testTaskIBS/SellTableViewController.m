//
//  SellTableViewController.m
//  testTaskIBS
//
//  Created by Александр Волков on 20.04.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

#import "SellTableViewController.h"
#import "DetailSellTableViewController.h"


@interface SellTableViewController ()


@end

@implementation SellTableViewController

- (IBAction)unwindToSellVC:(UIStoryboardSegue *) segue {
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    
    self.navigationItem.title = @"Продать";
    [self.navigationController.navigationBar setTintColor:[UIColor redColor]];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:true];
    
    [self.tableView reloadData];

    

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [ProductST sharedProductST].list.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Sell Cell" forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Sell Cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    Product *item = [ProductST sharedProductST].list[indexPath.row];
    cell.textLabel.text = item.name;
    
    cell.detailTextLabel.text = item.price;
    
    return cell;
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DetailSellTableViewController *dstvc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier: @"DetailSellVC"];
    [self.navigationController pushViewController:dstvc animated:true];
    Product *item = [ProductST sharedProductST].list[indexPath.row];
    dstvc.tag = [NSNumber numberWithInteger:indexPath.row];
    dstvc.product = item;
    
    
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    [[ProductST sharedProductST] removeProductAtIndex: indexPath.row];
    [tableView reloadData];
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqual: @"add product"]) {
        DetailSellTableViewController *dtvc = segue.destinationViewController;
        Product *item = [[Product alloc] init];
        dtvc.product = item;
        
        
    }
}



@end

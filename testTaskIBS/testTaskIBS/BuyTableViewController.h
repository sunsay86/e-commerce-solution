//
//  BuyTableViewController.h
//  testTaskIBS
//
//  Created by Александр Волков on 19.04.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"
#import "ProductST.h"
@interface BuyTableViewController : UITableViewController
@property (strong,nonatomic,readwrite) Product *product;
@end
